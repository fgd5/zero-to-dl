#Zero-to-DL
Este workshop es una introduccion mitad teorica mitad practica al deep learning y las redes neuronales convolucionales (CNN). 
Esta orientado a personas que no han tratado nunca con CNN y no tienen o tienen poco conocimiento sobre machine learning en general.
Se incluyen unas transparencias sobre las bases teoricas de ML y CNN (1 hora) y una serie de ejemplos ya programados para entrenar y testear diferentes tipos de CNN.

#Que se incluye
- 1-mnist: Pipeline completo de entrenamiento, testeo y evaluaci�n de un multilayer perceptron y de una CNN muy muy simple para reconocer digitos manuscritos.
- 2-object-classification: Ejemplo de uso de una CNN mas compleja (ResNet50) con imagenes de disco y con la webcam del ordenador que es capaz de diferenciar entre 1000 tipos de objetos diferentes.
- 3-object-loc: Ejemplo de uso de SDD, que es capaz de detectar que objetos aparecen en la imagen y su localizacion. El modelo que se incluye es capaz de detectar 15 tipos de objetos diferentes. Ejemplo con imagenes de disco y con la webcam del ordenador.

#Requisitos
- Ganas de aprender y de participar
- Python (en Ubuntu ya viene instalado)
- Tensorflow (`pip install tensorflow==1.4 --user`)
- Keras (`pip install Keras==2.1.3 --user`)
- matplotlib (`pip install matplotlib --user`)
- OpenCV (`sudo apt-get install python-opencv`)

#Como descargo esto?
`git clone --depth=1 https://bitbucket.org/fgd5/zero-to-dl.git`

#Benchmark
|Metodo|Fase|Tiempo(s/ep)|Setup|
|------|----|------------|-----|
|1-1 multi layer perceptron|Train|11|i3-3120M|
|1-1 multi layer perceptron|Train|<1|GTX1080Ti|
|1-1 multi layer perceptron|Test|<1|i3-3120M|
|1-1 multi layer perceptron|Test|<1|GTX1080Ti|
|1-2 baseline|Train|188|i3-3120M|
|1-2 baseline|Train|<1|GTX1080Ti|
|1-2 baseline|Test|<1|i3-3120M|
|1-2 baseline|Test|<1|GTX1080Ti|
|1-2 larger|Train|250|i3-3120M|
|1-2 larger|Train|<1|GTX1080Ti|
|1-2 larger|Test|<1|i3-3120M|
|1-2 larger|Test|<1|GTX1080Ti|
|2 ResNet50|Test|1.1|i3-3120M|
|2 ResNet50|Test|<1|GTX1080Ti|
|3 SSD300|Test|2.88|i3-3120M|
|3 SSD300|Test|<1|GTX1080Ti|

#FAQ
> Me tarda mucho en ejecutar o se me queda bloqueado

Las redes neuronales convolucionales involucran millones de operaciones, es normal que tarde un poco dependiendo dela potencia de tu CPU. En el mundo real, las operaciones se aceleran usando una o varias GPU (Mira la seccion Benchmark). 

##�Te has quedado con ganas de mas o tienes dudas?
Francisco Gomez-Donoso fgomez@ua.es 
# Example based on https://github.com/pierluigiferrari/ssd_keras

from keras import backend as K
from keras.models import load_model
from keras.preprocessing import image
from keras.optimizers import Adam
import numpy as np
from matplotlib import pyplot as plt
from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
import cv2

classes = ['background',
           'aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat',
           'chair', 'cow', 'diningtable', 'dog',
           'horse', 'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor']

inputImage = 'images/fish_bike.jpg'

# create the network
print "Creating the network..."
model = ssd_300(image_size=(300, 300, 3),
                n_classes=20,
                mode='inference',
                l2_regularization=0.0005,
                scales=[0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05], # The scales for MS COCO are [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
                aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5]],
                two_boxes_for_ar1=True,
                steps=[8, 16, 32, 64, 100, 300],
                offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                clip_boxes=False,
                variances=[0.1, 0.1, 0.2, 0.2],
                normalize_coords=True,
                subtract_mean=[123, 117, 104],
                swap_channels=[2, 1, 0],
                confidence_thresh=0.5,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400)
# load a pretrained model on the imagenet dataset
model.load_weights('weights/VGG_VOC0712_SSD_300x300_iter_120000.h5', by_name=True)
print "done"

# load an image. Note that the input size is 300x300
imgOri = cv2.imread(inputImage)
img = np.copy(cv2.resize(imgOri, (300, 300)) )
img = image.img_to_array(img[...,::-1])
# use the network to predict
y_pred = model.predict(img.reshape(1,300,300,3))

# the output of every network is fixed so it predicts always the same number of objects and boxes.
# nonetheless, we can get the interesting items by looking at their confidence score
# a big amount of the prediction will be nonsense so we apply a nms with a threshold
confidence_threshold = 0.5
y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]

# visualization of the predictions
for reg in y_pred_thresh[0]:
    print reg
    xmin = int(reg[2] * imgOri.shape[1] / 300)
    ymin = int(reg[3] * imgOri.shape[0] / 300)
    xmax = int(reg[4] * imgOri.shape[1] / 300)
    ymax = int(reg[5] * imgOri.shape[0] / 300)
    cv2.rectangle(imgOri, (xmin,ymin),(xmax,ymax),(0,255,0),1)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(imgOri,classes[int(reg[0])],(xmin,ymin+10), font, 0.5,(0,0,0))

cv2.imshow("Image", imgOri)
cv2.waitKey(0)
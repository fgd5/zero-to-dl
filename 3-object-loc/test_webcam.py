# Example based on https://github.com/pierluigiferrari/ssd_keras

from keras import backend as K
from keras.models import load_model
from keras.preprocessing import image
from keras.optimizers import Adam
import numpy as np
from matplotlib import pyplot as plt
from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
import cv2
from threading import *
import time

classes = ['background',
           'aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat',
           'chair', 'cow', 'diningtable', 'dog',
           'horse', 'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor']

# create the network
print "Creating the network..."
model = ssd_300(image_size=(300, 300, 3),
                n_classes=20,
                mode='inference',
                l2_regularization=0.0005,
                scales=[0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05], # The scales for MS COCO are [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
                aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                         [1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5]],
                two_boxes_for_ar1=True,
                steps=[8, 16, 32, 64, 100, 300],
                offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                clip_boxes=False,
                variances=[0.1, 0.1, 0.2, 0.2],
                normalize_coords=True,
                subtract_mean=[123, 117, 104],
                swap_channels=[2, 1, 0],
                confidence_thresh=0.5,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400)
# load a pretrained model on the imagenet dataset
model.load_weights('weights/VGG_VOC0712_SSD_300x300_iter_120000.h5', by_name=True)
print "done"

# ignore this dirty overkill method to capture webcam images
imageFromWebcam = None
capturing = True
def retrieveFromWebcam():
    global imageFromWebcam, capturing
    cam = cv2.VideoCapture(0)  #set the port of the camera as before
    while capturing:
        retval, image = cam.read() #return a True boolean and and the image if all go right
        imageFromWebcam = np.copy(image)
    cam.release() #Closes video file or capturing device.

th = Thread(target=retrieveFromWebcam)
th.start()
time.sleep(3)


confidence_threshold = 0.5

while capturing:
    try:
        # actually load the network. Note that opencv reads the images in BGR format but the networks expects it to be RGB
        # also we resize the images to 300x300, which is the input size of this network
        img = cv2.resize(imageFromWebcam, (300, 300))
        img = image.img_to_array(img[...,::-1])
        # use the network to predict
        y_pred = model.predict(img.reshape(1,300,300,3))
        # apply NMS
        y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]

        # visualization of the predictions
        for reg in y_pred_thresh[0]:
            print reg
            xmin = int(reg[2] * imageFromWebcam.shape[1] / 300)
            ymin = int(reg[3] * imageFromWebcam.shape[0] / 300)
            xmax = int(reg[4] * imageFromWebcam.shape[1] / 300)
            ymax = int(reg[5] * imageFromWebcam.shape[0] / 300)
            cv2.rectangle(imageFromWebcam, (xmin,ymin),(xmax,ymax),(0,255,0),1)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(imageFromWebcam,classes[int(reg[0])],(xmin,ymin+10), font, 0.5,(0,255,0))

        cv2.imshow("webcam",imageFromWebcam)
        cv2.waitKey(100)

    except KeyboardInterrupt:
        capturing = False


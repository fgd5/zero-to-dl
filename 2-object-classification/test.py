# Examples based on https://www.kaggle.com/gaborfodor/resnet50-example#

import numpy as np
import matplotlib.pyplot as plt
from os import makedirs
from os.path import join, exists, expanduser
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.applications.imagenet_utils import decode_predictions

inputImage = 'sample_imgs/dog.jpg'

# create the network
print "Creating the network..."
resnet = ResNet50(weights=None)
# load a pretrained model on the imagenet dataset
resnet.load_weights('models/resnet50_weights_tf_dim_ordering_tf_kernels.h5')
print "done"

# load an image. Note that the input size is 224 x 224
img = image.load_img(inputImage, target_size=(224, 224))
img = image.img_to_array(img)
# preprocess the image (resizing, mean sub, stdev div...)
x = preprocess_input(np.expand_dims(img.copy(), axis=0))
# use the network to predict
preds = resnet.predict(x)
print preds
print preds.shape
dec_preds = decode_predictions(preds, top=5)

print dec_preds
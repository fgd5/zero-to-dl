
import numpy as np
import matplotlib.pyplot as plt
from os import makedirs
from os.path import join, exists, expanduser
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.applications.imagenet_utils import decode_predictions
import cv2
from threading import *
import time


# create the network 
print "Creating the network..."
resnet = ResNet50(weights=None)
# load a pretrained model on the imagenet dataset
resnet.load_weights('models/resnet50_weights_tf_dim_ordering_tf_kernels.h5')
print "done"

# ignore this dirty overkill method to capture webcam images
imageFromWebcam = None
capturing = True
def retrieveFromWebcam():
	global imageFromWebcam, capturing
	cam = cv2.VideoCapture(0)  #set the port of the camera as before
	while capturing:
		retval, image = cam.read() #return a True boolean and and the image if all go right
		imageFromWebcam = np.copy(cv2.resize(image, (224, 224)) )
	cam.release() #Closes video file or capturing device.

th = Thread(target=retrieveFromWebcam)
th.start()
time.sleep(3)


while capturing:
	try:
		# actually load the network. Note that opencv reads the images in BGR format but the networks expects it to be RGB
		img = image.img_to_array(imageFromWebcam[...,::-1])
		# preprocess the image (resizing, mean sub, stdev div...)
		x = preprocess_input(np.expand_dims(img.copy(), axis=0))
		# use the network to predict
		preds = resnet.predict(x)

		dec_preds = decode_predictions(preds, top=5)

		print dec_preds
		cv2.imshow("webcam",imageFromWebcam)
		cv2.waitKey(100)

	except KeyboardInterrupt:
		capturing = False
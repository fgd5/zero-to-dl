# Example based on https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/

from keras.datasets import mnist
import matplotlib.pyplot as plt
import numpy
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils
import random

# load (downloaded if needed) the MNIST dataset
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# plot 4 images as gray scale
plt.subplot(221)
plt.imshow(X_train[0], cmap=plt.get_cmap('gray'))
plt.subplot(222)
plt.imshow(X_train[1], cmap=plt.get_cmap('gray'))
plt.subplot(223)
plt.imshow(X_train[2], cmap=plt.get_cmap('gray'))
plt.subplot(224)
plt.imshow(X_train[3], cmap=plt.get_cmap('gray'))
# show the plot
plt.show()


# load data
(X_train, y_train), (X_test, y_test) = mnist.load_data()
print(str(X_train.shape)+" samples for training")
print(str(X_test.shape)+" samples for testing")

# flatten 28*28 images to a 784 vector for each image
num_pixels = X_train.shape[1] * X_train.shape[2]
X_train = X_train.reshape(X_train.shape[0], num_pixels).astype('float32')
X_test = X_test.reshape(X_test.shape[0], num_pixels).astype('float32')

#normalize inputs from 0-255 to 0-1
X_train = X_train / 255
X_test = X_test / 255

# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(num_pixels, input_dim=num_pixels, kernel_initializer='normal', activation='relu'))
	model.add(Dense(num_classes, kernel_initializer='normal', activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

# build the model
model = baseline_model()
# Fit the model
history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)
# Final evaluation of the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("Baseline Error: %.2f%%" % (100-scores[1]*100))


fig = plt.figure()
plt.subplot(121)
plt.plot(history.history["loss"]);
plt.plot(history.history["val_loss"]);
plt.ylim(0,1)
plt.xlabel('Epochs')
plt.legend(['Taining Loss','Validation Loss'])
plt.subplot(122)
plt.plot(history.history["acc"]);
plt.plot(history.history["val_acc"]);
plt.ylim(0,1)
plt.xlabel('Epochs')
plt.legend(['Taining Accuracy','Validation Accuracy'])
plt.show()


# testing some random images from the test split
for i in range(10):
	n = random.randint(0,X_test.shape[0])
	x = X_test[n]
	preds = model.predict(x.reshape(1,num_pixels))
	print preds
	print "It was a",y_test[n].argmax(),"and was predicted as a", preds[0].argmax()
	print "-----------------------------------------------------------------------------"
	plt.imshow(x.reshape(28,28), cmap=plt.get_cmap('gray'))
	plt.show()
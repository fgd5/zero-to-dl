# Example based on https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/

import matplotlib.pyplot as plt
import numpy
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.utils import np_utils
import random

# load (downloaded if needed) the MNIST dataset
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# plot 4 images as gray scale
plt.subplot(221)
plt.imshow(X_train[0], cmap=plt.get_cmap('gray'))
plt.subplot(222)
plt.imshow(X_train[1], cmap=plt.get_cmap('gray'))
plt.subplot(223)
plt.imshow(X_train[2], cmap=plt.get_cmap('gray'))
plt.subplot(224)
plt.imshow(X_train[3], cmap=plt.get_cmap('gray'))
# show the plot
plt.show()


# load data
(X_train, y_train), (X_test, y_test) = mnist.load_data()
# reshape to be [samples][pixels][width][height]
X_train = X_train.reshape(X_train.shape[0], 1, 28, 28).astype('float32')
X_test = X_test.reshape(X_test.shape[0], 1, 28, 28).astype('float32')

print(str(X_train.shape)+" samples for training")
print(str(X_test.shape)+" samples for testing")


#normalize inputs from 0-255 to 0-1
X_train = X_train / 255
X_test = X_test / 255

# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.2))
	model.add(Flatten())
	model.add(Dense(128, activation='relu'))
	model.add(Dense(num_classes, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

def larger_model():
	# create model
	model = Sequential()
	model.add(Conv2D(30, (5, 5), input_shape=(1, 28, 28), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Conv2D(15, (3, 3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.2))
	model.add(Flatten())
	model.add(Dense(128, activation='relu'))
	model.add(Dense(50, activation='relu'))
	model.add(Dense(num_classes, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

# build the model
model = baseline_model() # larger_model()
# Fit the model
history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)
# Save the trained model
model.save_weights("model_larger.h5")
# or load a pretrained model
#model.load_weights("model_base.h5")
# Final evaluation of the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("Simple CNN Error: %.2f%%" % (100-scores[1]*100))


fig = plt.figure()
plt.subplot(121)
plt.plot(history.history["loss"]);
plt.plot(history.history["val_loss"]);
plt.ylim(0,1)
plt.xlabel('Epochs')
plt.legend(['Taining Loss','Validation Loss'])
plt.subplot(122)
plt.plot(history.history["acc"]);
plt.plot(history.history["val_acc"]);
plt.ylim(0,1)
plt.xlabel('Epochs')
plt.legend(['Taining Accuracy','Validation Accuracy'])
plt.show()


# testing some random images from the test split
for i in range(10):
	n = random.randint(0,X_test.shape[0])
	x = X_test[n]
	preds = model.predict(x.reshape(1,1,28,28))
	print preds
	print "It was a",y_test[n].argmax(),"and was predicted as a", preds[0].argmax()
	print "-----------------------------------------------------------------------------"
	plt.imshow(x[0], cmap=plt.get_cmap('gray'))
	plt.show()